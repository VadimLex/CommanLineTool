//
//  Console.hpp
//  CommanLineTool
//
//  Created by Vadim Aitov on 01.05.2022.
//
#include <cmath>
#ifndef Console_hpp
#define Console_hpp
#include <stdio.h>
#endif /* Console_hpp */



// Функция по определению суммы 2-х чисел а и b
double Fsum(double a, double b)
{
    return a + b;
};
// Функция по определению квадрата суммы 2-х чисел а и b
double KvSummi(double x, double y)
{
    return pow((x + y), 2);
}
// Функция по выводу четных чисел в диапзане от 0 до n

int FuncCoutEvenNubres(int n)
{
    std::cout << "Четные цифры: \n";
for (int i = 0; i <= n; ++i)
    {
        std::cout << i << "\n";
        ++i;
    }
    return 0;
}
// Функция по выводу нечетных чисел в диапзане от 0 до n
int FuncCoutOddNubres(int n)
{
    std::cout << "Нечетные цифры: \n";
for (int i = 1; i <= n; ++i)
    {
        std::cout << i << "\n";
        ++i;
    }
    return 0;
}

