//
//
//  Mate.cpp
//  Mate
//
//  Created by Vadim Aitov on 25.04.2022.
//
#include <cmath>
#include <iostream>
#include "Console.hpp"
#include <stdint.h>
#include <iomanip>
#include <string>
// Ввод даты
#include <time.h>
using namespace std;

// Объявление Define
#define PI 3.14
#define LOG

// Пространства имён в глобальном
//namespace A
//{
//    void a()
//    {
//        std::cout << "a \n";
//    }
//
//    namespace AB
//    {
//        void a()
//        {
//            std::cout << "ab \n";
//        }
//    }
//}

//void a()
//{
//    std::cout << "b \n";
//}

//namespace AAB = A::AB;
//using namespace A;
//using A::a;

//void calc()
//{
//    using namespace A;
//    a();
//}



    

int main()
{
    
    
    // Задание 16.5
    std::cout << "Lesson  16.5 \n Введите количество N элементов массива array [N][N]: ";
    int n;
    std::cin >> n;
    
    int array[n][n];
    
    for (int i = 0; i < n; i++)
     {
         for (int j = 0; j < n; j++)
         {
             array[i][j] = i+j;
             std::cout << array[i][j];
         }
         std::cout << "\n";
     }
    // Ввод даты
    time_t now = time(0);

 //   cout << "Number of sec since January 1,1900:" << now << endl;

    tm *ltm = localtime(&now);

    // print various components of tm structure.
//    cout << "Year " << 1900 + ltm->tm_year<<endl;
//    cout << "Month: "<< 1 + ltm->tm_mon<< endl;
//    cout << "Day: "<<  ltm->tm_mday << endl;
//    cout << "Time: "<< ltm->tm_hour << ":";
//    cout << 1 + ltm->tm_min << ":";
//    cout << 1 + ltm->tm_sec << endl;
    
    int Today = ltm->tm_mday;

    int RemainderOfDivision = Today % n;
    std::cout << " Сегодня: " << Today << " число. Остаток от деления этой даты на число элементов массива: " <<  n << " равно " << RemainderOfDivision << "\n";
 // Сумма строки в массиве array[RemainderOfDivision][j]
    int Summa = 0;
    for (int j = 0; j < n; j++)
    {
        Summa = Summa + array[RemainderOfDivision][j];
    }
    std::cout << " Сумма элементов в строке " << RemainderOfDivision << " массива : " << Summa << "\n";
    
    
    
    // 16.4 Преобразование типов данных
    
//    bool a = false;
//    std::cout << (a || true);
    
  //  std::cout << static_cast<double>(5);
 //   auto a = 5.0;
    // По убываниюприоритета
/*
    long double
    double
    float
    unsigned long long
    long long
    unsigned long
    long
    unsigned int
    int
*/
    
    
    
    // Пространства имён в локальном
//    A::AB::a();
//    AAB::a();
//    a();
//    a();
//    calc();
    
    
    
    
    // Массивы

//    const int size = 2;
//    int array [size] = {0,1};
////    std::cout << array[0] << "\n";
//
//    for (int i=0; i < size; i++)
//    {
//        std::cout << array[i] << "\n";
//    }
    
//    int array [size] [size] = {{0,1}, {2,3}};
//    for (int i = 0; i < size; i++)
//    {
//        for (int j = 0; j < size; j++)
//        {
//            std::cout << array[i][j];
//        }
//        std::cout << "\n";
//    }
    

    
    
    
    //    static int n;
//    std::cout << "Lesson  15.4 \n Выводим последовательность четных чисел \n Ввдите любое число: ";
//    std::cin >> n;
//    FuncCoutEvenNubres(n);
//    std::cout << "Lesson  15.4 \n Выводим последовательность нечетных чисел \n Ввдите любое число: ";
//    std::cin >> n;
//    FuncCoutOddNubres(n);
    
 
    
    
    
    //Циклы
    
//    for (int i = 0; i < 10; ++i)
//    {
//        std::cout << "i = " << i << "\n";
//        if (i < 5)
//        {
////            break;
//            continue;
//        }
//        std::cout << "i*i = " << i * i << "\n";
//    }
//   int i = 0;
//    while (i<5)
//    {
//        i++;
//        std::cout << "aaaa" << "\n";
//    }
    
 
    
    
    
    //Выражения switch
//    int a = 8;
//   const  int b = 1;
//    switch (a) {
//        case b : //if a==1
//            std::cout << "a = 1" << "\n";
//            break;
//        case 8 : //if a==8
//            std::cout << "a = 8" << "\n";
//            break;
//
//        default:
//            std::cout << "a != 1 & a!= 8  => default" << "\n";
//            break;
//    }
    
  
    
    
    
    
    // Выражения if/else
//    if(a!= 8)
//    {
//        std::cout << "true = " << true << "\n";
//    }
//    else if (a==8)
//    {
//        std::cout << "else! " << "\n";
//    }
//
//    int a = 8;
//    int b(7);
//    int q3{30};
    
   
    
    
    
    
    // Выражения сравнения, сложения, равенства итд
//    a += b; //a = a + b;
//    std::cout << a << "\n";
    
//    std::cout << a + b << "\n";
//    std::cout << a - b << "\n";
//    std::cout << a * b << "\n";
//    std::cout << a / b << "\n";
//    std::cout << b % a << "\n";
//
//    int x = 0;
//    std::cout << "x = " << x << "\n";
//    int y = x++;
//    std::cout << "x = " << x << " y = x++ = " << y << "\n";
//    y = ++x;
//    std::cout << "x = " << x << " y = x++ = " << y << "\n";
    
//    bool c = 0 < 1; // >, <, =, !=, >=, <=
//    bool m = false;
//    std::cout << "\n c = " << c << " m = " << m << "\n";
//    std::cout << "\n c && m = " << (c && m) << "\n";
//    std::cout << "\n c || m = " << (c || m) << "\n";
//    std::cout << "\n !m = " << !m << "\n";
//    std::cout << "\n !(c && m) = " << !(c && m) << "\n";
//

    
    
    
    
    
    //    double q4{40.0}, q5{500};
//
//    int q = q1 + q2 + q3 +q4 + q5;

//    bool a1 = true; // Занимает в память 1 байт!
//    bool a2 = false; // Занимает в память 1 байт!
//
//    char a3 = '\n'; // Занимает в память 1 байт! символьный тип данных - может хранить даже перевод строки -
//    //его надо ставить в одинарные кавычки -так  помещаются символы
//    // а строки - в двойные: " строка"
//    // Условно можно отнести к типу int - сюда можно поместить букву или число от -128 до 127.
//
//    short a4 =5; // Занимает в память 2 байта! Используется для целочисленных данных как int, но занимает 2 байта
//    // Может  хранить -32700 до 32700
//    int a5 = 100; // Занимает в память 4 байта! Может  хранить -2 млрд до 2 млрд.
//
//    long a6 = 500; // Занимает в память 4 байта! Может  хранить -2 млрд до 2 млрд.
//    long long a7 = 100000000000; // Занимает в память 8 байт!
//
//    float b1 = 0.00000000000009;
//    double b2 = 0.00000000000000000000009; // в 2 раза больше значений после запятой
//    long double b3 = 0.33333333333333333333333333333333; //  в 2 раза больше значений после запятой чем double
    
    
//    std::cout << "Размер float в байтах: " << sizeof(float) << "\n";  // Определяем размер памяти для типа переменной

//    int8_t k1 = 1; //от -128 до 127
//    uint8_t k2 = 255; // от 0 до 255
//    int16_t k3 = 32000; //от -32700 до 32700
//    uint16_t k4 = 65000; // от 0 до 65000
//    int32_t k5 = 65000;
//    uint32_t k6 = 65000;
//    int64_t k7 = 65000;
//    uint64_t k8 = 65000; //18 15 нулей
//    // целые в зависимости от количества бит

//    int8_t number = 44; // выводит запятую! Лучше им не пользоваться для вывода чисел, а использовать int64_t;
//        std::cout << "Number = " << number << q << std::endl;
 
    // 5 в степени -100!!!!
//    float d = 5e-2;
//        std::cout << "5e-2 = " << d << std::endl;
//    double dd = 5e-10; // 5 в степени -10 в 10-й!
//        std::cout  << std::fixed << std::setprecision(10) << "5e-10 = " << dd << std::endl;
//        std::cout  << std::setprecision(10) << "5e-10 = " << dd << std::endl;
//        std::cout  << std::fixed << "5e-10 = " << dd << std::endl;
//        std::cout  << "5e-10 = " << dd << std::endl;

    
    
    
    
    //   //False -True
//    bool str = true;
//
//    if (str)
//    {
//        std::cout << "100 < 200" << number << std::endl;
//    }
//    else
//    {
//        std::cout << "False = " << number << std::endl;
//    }
    
    
    
    //Ввод с клавиатуры
    //    std::cin >> name; //ввод и присвоение значения с клавиатуры в name

    
    
    
//    std::cout << "Lesson  14.4 \n Enter your name: ";
//    std::string name, age;
//    std::getline(std::cin, name);
//    std::cout << " Enter your age: ";
//    std::getline(std::cin, age);
//
//     std::cout << "\n Salute,  " << name << ". Your age: " << age <<" yers old.\n";
//    // Определение длинны переменной name
//    long dlina = name.length();
//        std::cout << "\n Длинна переменной Имя (name) : " << dlina << " символов. \n";
//    char sim = name[0];
//        std::cout << "Первый символ в имени : " << sim << "\n";
//    char sim1 = name[dlina-1];
//        std::cout << "Последний символ в имени : " << sim1 << "\n";

    
    
    
//Сложение строковых
//    std::string first = "10hs  xhxh", second = "jksjnsnsxbx   bshshj";
//    std::cout << first + second << "\n";
//    std::cout << first.length() << "\n";
//    std::cout << second.length() << "\n";
    //
    
    
    
    //Вывод define
//    std::cout <<"pi= " << PI << std::endl;
    //
    // Вывод define по условию
//#ifdef LOG
//     std::cout <<"Результат LOG " << std::endl;
//#endif
    
    
    
    
// Квадрат суммы
//    std::cout <<"\n \n Lesson 13.4 \n" << std::endl;
//    double x1, y1;
//    std::cout << "Enter any integer X: ";
//    std::cin >> x1;
//    std::cout << "Enter any integer Y: ";
//    std::cin >> y1;
//
//    double result = Fsum(x1, y1);
//    std::cout <<"Result function Fsum: Cуммы чисел X= " << x1 << " и Y= " << y1 << " равно = " << result << std::endl;
//
//    std::cout <<"Result function KvSum. Квадрат суммы чисел X= " << x1 << " и Y= " << y1 << " равно = " << KvSummi(x1, y1)  << std::endl;
//    return 0;
}
