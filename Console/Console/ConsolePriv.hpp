//
//  ConsolePriv.hpp
//  Console
//
//  Created by Vadim Aitov on 01.05.2022.
//

/* The classes below are not exported */
#pragma GCC visibility push(hidden)

class ConsolePriv
{
    public:
    void HelloWorldPriv(const char *);
};

#pragma GCC visibility pop
