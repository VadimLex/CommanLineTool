//
//  Console.cpp
//  Console
//
//  Created by Vadim Aitov on 01.05.2022.
//

#include <iostream>
#include "Console.hpp"
#include "ConsolePriv.hpp"

void Console::HelloWorld(const char * s)
{
    ConsolePriv *theObj = new ConsolePriv;
    theObj->HelloWorldPriv(s);
    delete theObj;
};

void ConsolePriv::HelloWorldPriv(const char * s) 
{
    std::cout << s << std::endl;
};

