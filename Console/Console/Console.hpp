//
//  Console.hpp
//  Console
//
//  Created by Vadim Aitov on 01.05.2022.
//

#ifndef Console_
#define Console_

/* The classes below are exported */
#pragma GCC visibility push(default)

class Console
{
    public:
    void HelloWorld(const char *);
};

#pragma GCC visibility pop
#endif
